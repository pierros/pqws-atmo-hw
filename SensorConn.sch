EESchema Schematic File Version 4
LIBS:pqws-atmo-hw-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 8
Title "PQ9ISH Sensor Module"
Date "2018-04-30"
Rev ""
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pq-atmo-connectors:PQ-Sensor J3
U 1 1 5AE773C8
P 4200 3600
AR Path="/5AE9F968/5AE773C8" Ref="J3"  Part="1" 
AR Path="/5AE772E0/5AE773C8" Ref="J1"  Part="1" 
AR Path="/5AE77977/5AE773C8" Ref="J2"  Part="1" 
AR Path="/5AEA0DE2/5AE773C8" Ref="J4"  Part="1" 
F 0 "J4" H 4120 3175 50  0000 C CNN
F 1 "SensorCon" H 4120 3266 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B4B-PH-K_1x04_P2.00mm_Vertical" H 4200 3600 50  0001 C CNN
F 3 "~" H 4200 3600 50  0001 C CNN
	1    4200 3600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5AE77469
P 4550 3850
AR Path="/5AE9F968/5AE77469" Ref="#PWR013"  Part="1" 
AR Path="/5AE772E0/5AE77469" Ref="#PWR09"  Part="1" 
AR Path="/5AE77977/5AE77469" Ref="#PWR011"  Part="1" 
AR Path="/5AEA0DE2/5AE77469" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 4550 3600 50  0001 C CNN
F 1 "GND" H 4555 3677 50  0000 C CNN
F 2 "" H 4550 3850 50  0001 C CNN
F 3 "" H 4550 3850 50  0001 C CNN
	1    4550 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3600 5500 3600
Wire Wire Line
	4950 3600 4400 3600
Wire Wire Line
	4400 3700 4550 3700
Wire Wire Line
	4550 3700 4550 3850
Text HLabel 4400 3400 2    50   Input ~ 0
S_PinA
Text HLabel 4400 3500 2    50   Input ~ 0
S_PinB
Text HLabel 5700 4000 2    50   Input ~ 0
S_Power
$Comp
L lsd-kicad:Si1401EDH Q3
U 1 1 5AE99F31
P 5150 3700
AR Path="/5AE9F968/5AE99F31" Ref="Q3"  Part="1" 
AR Path="/5AE772E0/5AE99F31" Ref="Q1"  Part="1" 
AR Path="/5AE77977/5AE99F31" Ref="Q2"  Part="1" 
AR Path="/5AEA0DE2/5AE99F31" Ref="Q4"  Part="1" 
F 0 "Q4" V 5775 3700 50  0000 C CNN
F 1 "Si1401EDH" V 5684 3700 50  0000 C CNN
F 2 "lsf-kicad-lib:SOT-563" H 5350 3800 50  0001 C CNN
F 3 "http://www.vishay.com/docs/70080/si1401ed.pdf" H 5150 3700 50  0001 C CNN
	1    5150 3700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R7
U 1 1 5AE9A012
P 5500 3850
AR Path="/5AE9F968/5AE9A012" Ref="R7"  Part="1" 
AR Path="/5AE772E0/5AE9A012" Ref="R5"  Part="1" 
AR Path="/5AE77977/5AE9A012" Ref="R6"  Part="1" 
AR Path="/5AEA0DE2/5AE9A012" Ref="R8"  Part="1" 
F 0 "R8" H 5570 3896 50  0000 L CNN
F 1 "100K" H 5570 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.99x1.00mm_HandSolder" V 5430 3850 50  0001 C CNN
F 3 "~" H 5500 3850 50  0001 C CNN
	1    5500 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4000 5500 4000
Wire Wire Line
	5150 4000 5150 3900
Wire Wire Line
	5500 4000 5150 4000
Wire Wire Line
	5500 3700 5500 3600
Connection ~ 5500 3600
Wire Wire Line
	5500 3600 5350 3600
Wire Wire Line
	4950 3300 4950 3400
Connection ~ 4950 3600
Connection ~ 4950 3400
Wire Wire Line
	4950 3400 4950 3500
Connection ~ 4950 3500
Wire Wire Line
	4950 3500 4950 3600
Connection ~ 5500 4000
$Comp
L Device:Jumper_NC_Dual JP5
U 1 1 5AEA0229
P 6400 3600
AR Path="/5AE9F968/5AEA0229" Ref="JP5"  Part="1" 
AR Path="/5AE772E0/5AEA0229" Ref="JP3"  Part="1" 
AR Path="/5AE77977/5AEA0229" Ref="JP4"  Part="1" 
AR Path="/5AEA0DE2/5AEA0229" Ref="JP6"  Part="1" 
F 0 "JP6" V 6354 3701 50  0000 L CNN
F 1 "Jumper_NC_Dual" V 6445 3701 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 6400 3600 50  0001 C CNN
F 3 "~" H 6400 3600 50  0001 C CNN
	1    6400 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 3950 6400 3950
Wire Wire Line
	6400 3950 6400 3850
$Comp
L power:+3.3V #PWR014
U 1 1 5AEA0468
P 6400 3250
AR Path="/5AE9F968/5AEA0468" Ref="#PWR014"  Part="1" 
AR Path="/5AE772E0/5AEA0468" Ref="#PWR010"  Part="1" 
AR Path="/5AE77977/5AEA0468" Ref="#PWR012"  Part="1" 
AR Path="/5AEA0DE2/5AEA0468" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 6400 3100 50  0001 C CNN
F 1 "+3.3V" H 6415 3423 50  0000 C CNN
F 2 "" H 6400 3250 50  0001 C CNN
F 3 "" H 6400 3250 50  0001 C CNN
	1    6400 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3250 6400 3350
Text Label 5750 3600 0    50   ~ 0
VSensIn
Text Label 4550 3600 0    50   ~ 0
VSensSW
$Comp
L power:VCC #PWR07
U 1 1 5AEAE553
P 6500 3950
AR Path="/5AE772E0/5AEAE553" Ref="#PWR07"  Part="1" 
AR Path="/5AE77977/5AEAE553" Ref="#PWR08"  Part="1" 
AR Path="/5AE9F968/5AEAE553" Ref="#PWR021"  Part="1" 
AR Path="/5AEA0DE2/5AEAE553" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 6500 3800 50  0001 C CNN
F 1 "VCC" V 6517 4078 50  0000 L CNN
F 2 "" H 6500 3950 50  0001 C CNN
F 3 "" H 6500 3950 50  0001 C CNN
	1    6500 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C3
U 1 1 5AEBDF82
P 4950 4200
AR Path="/5AE772E0/5AEBDF82" Ref="C3"  Part="1" 
AR Path="/5AE77977/5AEBDF82" Ref="C4"  Part="1" 
AR Path="/5AE9F968/5AEBDF82" Ref="C7"  Part="1" 
AR Path="/5AEA0DE2/5AEBDF82" Ref="C8"  Part="1" 
F 0 "C8" H 5065 4246 50  0000 L CNN
F 1 "C" H 5065 4155 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.39x1.80mm_HandSolder" H 4988 4050 50  0001 C CNN
F 3 "~" H 4950 4200 50  0001 C CNN
F 4 "GRM31CR60J157ME11L" H 4950 4200 50  0001 C CNN "Part Number"
	1    4950 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4050 4950 3600
$Comp
L power:GND #PWR023
U 1 1 5AEBE1ED
P 4950 4400
AR Path="/5AE772E0/5AEBE1ED" Ref="#PWR023"  Part="1" 
AR Path="/5AE77977/5AEBE1ED" Ref="#PWR024"  Part="1" 
AR Path="/5AE9F968/5AEBE1ED" Ref="#PWR025"  Part="1" 
AR Path="/5AEA0DE2/5AEBE1ED" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 4950 4150 50  0001 C CNN
F 1 "GND" H 4955 4227 50  0000 C CNN
F 2 "" H 4950 4400 50  0001 C CNN
F 3 "" H 4950 4400 50  0001 C CNN
	1    4950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4400 4950 4350
$EndSCHEMATC
